
public class ConstructorsInJava {

	// local
	// instance
	
	int a; // instance variable
	
	
	public  ConstructorsInJava(){
		System.out.println("in constructor");
	}
	
	
	public  ConstructorsInJava(int i){
		a=i;
		System.out.println("in param constructor");
	}
	
	public void init() {
		System.out.println(a);
		int a=25; //local
		System.out.println(a);
		System.out.println("in init");
	}
	
	
	public void show() {
		System.out.println(a);
	}

	public static void main(String[] args) {

		ConstructorsInJava obj = new ConstructorsInJava();
		
		ConstructorsInJava obj1 = new ConstructorsInJava(10);
		
		obj.show();
		obj1.show();
	}

}
