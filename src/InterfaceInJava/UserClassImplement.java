package InterfaceInJava;

import java.util.*;

public class UserClassImplement {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		
		WebDriverExample driver;
		
		String str = "chrome";		
		if(str.equals("chrome")) {
			driver = new ChromeDriverExample();			
		}else {
			driver = new FirefoxDriverExample();
		}		
		
		WebDriverExample obj = FactoryMethod.returnObject("CHROME");	
		
		
		
		driver.get();
		driver.close();
		driver.quit();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		
		

	}

}
