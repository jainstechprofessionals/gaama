package InterfaceInJava;

public class FactoryMethod {

	private FactoryMethod() {
		
	}
	
	
	public static WebDriverExample returnObject(String bank) {

		if (bank.equals("CHROME")) {
			return new ChromeDriverExample();
		} else  {
			return new FirefoxDriverExample();
		}
	}

}
