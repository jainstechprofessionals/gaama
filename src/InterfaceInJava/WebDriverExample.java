package InterfaceInJava;

public interface WebDriverExample {

	public void get();

	public String getTitle();

	public String getCurrentUrl();

	public void quit();

	public void close();

	default void show() {

	}

	static void print() {

	}
}
