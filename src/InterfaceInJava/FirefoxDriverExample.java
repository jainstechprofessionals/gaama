package InterfaceInJava;

public class FirefoxDriverExample implements WebDriverExample {

	
	FirefoxDriverExample(){
		System.out.println("firefox Launch");
	}
	
	@Override
	public void get() {
		System.out.println("in get");

	}

	@Override
	public String getTitle() {

		return "firefox";
	}

	@Override
	public String getCurrentUrl() {
		// TODO Auto-generated method stub
		return "firefox url";
	}

	@Override
	public void quit() {
		System.out.println("in quit firefox");

	}

	@Override
	public void close() {
		System.out.println("in close firefox");

	}

}
