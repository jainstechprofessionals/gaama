
public class ThisKeyWordInJava {

	int a;    // instance

	public ThisKeyWordInJava() {
		this(12);
		System.out.println("in default ThisKeyWordInJava");
		
	}

	public ThisKeyWordInJava(int a) {
		System.out.println("in parametrized");
		this.a = a;

	}

	public void ss(int a) {
		ThisKeyWordInJava obj = new ThisKeyWordInJava();
		obj.a = a;
	}

	public void show() {
		this.ss(12);
		System.out.println(a);
		int a = 10; //local
		System.out.println(this.a);
		System.out.println(a);
	}

	public static void main(String[] args) {

		ThisKeyWordInJava obj = new ThisKeyWordInJava();

	}

}
