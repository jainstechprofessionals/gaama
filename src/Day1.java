
public class Day1 {

	public static void main(String[] args) {
		
		MethodsInJava obj = new MethodsInJava();
		
		
		// Variable 
		
		int a;
		a=10;
		a=20;
		
		// Data type 
		
//		1. Primitive 
		
		boolean b = true ; // 1 bit
		char c = 'A'; // 2 byte
		
		byte by = 12; // 1 byte  (-128 to 127)
		short s = 1244; // 2 byte  (-32768 to 32767)
		int i = 145452 ; // 4 byte   (-2^31 to 2^31-1)
		long l = 243; // 8 byte  (-2^63 to 2^63-1)
		
		float f = 142.388888f; // 4 byte
		double d = 12.388888d; // 8 byte
		
	
//		2. Non primitive
		
		String str = "rounak";
	
		// Type Conversion
		
//		1. Implicit 
		
		s=by;
		
		
		f=i;
		System.out.println(f);
//		2. Explicit
		
		by=(byte) s;
		System.out.println(i);
		i=(int) f;
		
		System.out.println(i);
		
	}

}
