
public class StringInJava {

	public static void main(String[] args) {
	
		// by string literals
		String str = "rounak";
		String str1= "rounak";
		
		str1.concat(str);
		System.out.println(str);
		System.out.println(str1);
		
//		if(str==str1) {
//			System.out.println("equal");
//		}else {
//			System.out.println("not equal");
//		}
		
		if(str.equals(str1)) {
			System.out.println("equal");
		}else {
			System.out.println("not equal");
		}
		
		// by new Keyword
		
		String str2 = new String("rounak");		
		String str3= new String ("Rounak");
//		if(str2==str3) {
//			System.out.println("equal");
//		}else {
//			System.out.println("not equal");
//		}
		
		str3.concat(str2);
		System.out.println(str3);
		
		if(str2.equalsIgnoreCase(str3)) {
			System.out.println("equal");
		}else {
			System.out.println("not equal");
		}
	
		
		char c =str.charAt(3);
		System.out.println(c);
		
		// reverse the string
		
		String str4="pradeep";
		String temp = "   ";
		
		temp = temp + str4;
		System.out.println(temp );
		temp = temp+str4;
		System.out.println(temp );
		
		char arr[] = {'a','b','c'};
		String str5 = new String(arr);
		
		System.out.println(str5);
		
			

	}

}
