package InheritanceInJAva;

public class MainForAbstract {

	public static void main(String[] args) {
		
		PolicyBaazar s ;
		
		String str = "SBI";
		
		if(str.equalsIgnoreCase("SBI")) {
			s= new SBI();  // Run time polymor or Dynamic method Dispatch, upcasting,Late Binding, Dynamic binding
		}else {
			s= new ICICI();
		}
			
		s.getMutualFundDetails();
		s.getRateOfInterest();

	}

}
