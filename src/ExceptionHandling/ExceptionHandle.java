package ExceptionHandling;

public class ExceptionHandle {
	public static void main(String[] args) {

		try {
			int a = 9;
			int b = 10;
			System.out.println(a / b);
			String str = "null";
			System.out.println(str.length());
		} catch (ArithmeticException e) {
			e.printStackTrace();
			System.out.println("please enter a non zero value");

		} catch (NullPointerException e) {

			System.out.println("string is null");
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("please enter a non zero value");

		} 
		
		finally {
			System.out.println("in finally");
		}

	}

}
