package ExceptionHandling;

public class ExceptionHandle2 {
	public static void main(String[] args) {

		try {
			int a = 9;
			int b = 0;
			System.out.println(a / b);
			String str = null;
			System.out.println(str.length());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("please enter a non zero value");
		}
		finally {
			System.out.println("in finally");
		}

	}

}
