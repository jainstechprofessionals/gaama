package AccessModifier;

public class AccessMod1 {

	public void publicMethod() {
		System.out.println("in public ");
	}
	
	private void privateMethod() {
		System.out.println("in private");
	}
	
	protected void protectedMethod() {
		System.out.println("in protected");
	}
	
	void defaultMethod() {
		System.out.println("in default");
	}
	
	
	
	
	public static void main(String[] args) {
		
		AccessMod1 o= new AccessMod1();
		
	}

}
