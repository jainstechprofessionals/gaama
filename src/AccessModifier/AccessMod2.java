package AccessModifier;

public class AccessMod2 extends AccessMod1 {
 
	public void show() {
		super.defaultMethod();
		super.protectedMethod();
		super.publicMethod();
	}
	
	
}
