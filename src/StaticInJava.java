
public class StaticInJava {

//	static {
//		System.out.println("in static");
//		
//	}

	int rollNo;
	static String college = "LNCT";
	String name;

	public StaticInJava(int rollNo, String name) {
		this.rollNo = rollNo;
		this.name = name;

	}

	public static void display() {

		System.out.println("in display");
	}

	public void show() {
		System.out.println(rollNo);
		System.out.println(name);
		System.out.println(college);
	}

	public static void main(String[] args) {

		System.out.println("in main");
		StaticInJava obj = new StaticInJava(101, "rounak");

		StaticInJava obj1 = new StaticInJava(102, "avneet");

		StaticInJava obj2 = new StaticInJava(103, "pushpa");

		StaticInJava.display();

		obj.show();
		System.out.println("*************");
		obj.rollNo=23;
		StaticInJava.college="oriental";
		obj1.show();
		System.out.println("*************");
		obj2.show();

		System.out.println(Math.PI);

	}

}
